<div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Data Eval <?php echo strtoupper($kry->nama); ?> Kelas </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <!-- <a data-toggle="modal" href="#myModal" ><button class="btn btn-primary btn-flat">+ Tambah Data</button></a><hr> -->
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Parameter</th>
                        <th>Topik</th>
                        <th>Nilai</th>
                        <!-- <th width='80'>Aksi</th> -->
                      </tr>
                    </thead>
                    <tbody>
                      <?php $no = 1; foreach ($getData as $value) { ?>
                      <tr>
                        <td><?php echo number_format($no); ?>.</td>
                        <td><?php echo $value->parameter; ?></td>
                        <td><?php echo $value->topik; ?></td>
                        <?php $nilairat = $this->db->query("select AVG(nilai) as nilaibro from tbl_nilai_parameter where parameter_id = ".$value->id_parameter." and kd_jadwal = '".$this->session->userdata('kdjadwal')."'")->row()->nilaibro; 
                        //var_dump("select AVG(nilai) as nilaibro from tbl_nilai_parameter where parameter_id = ".$value->id_parameter." and kd_jadwal = '".$this->session->userdata('kdjadwal')."'");exit(); ?>
                        <td><?php echo number_format($nilairat,2); ?></td>
                        <!-- <td>
                          <div class="btn-group">
                            <button class="btn btn-success" type="button">Aksi</button>
                            <button class="btn btn-success dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="true">
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="#">Print</a></li>
                            </ul>
                          </div>
                        </td> -->
                      </tr>
                      <?php $no++; } ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->