<script>
function edit(id){
$('#edit').load('<?php echo base_url();?>admin/matakuliahedit/'+id);
}
</script>

<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Data Mata Kuliah</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            <a data-toggle="modal" href="#myModal" ><button class="btn btn-primary btn-flat">+ Tambah Data</button></a><hr>
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Kode MK</th>
                  <th>Mata Kuliah</th>
                  <th>SKS</th>
                  <th>Jurusan</th>
                  <th>Semester</th>
                  <th width='80'>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($getData as $value) { ?>
                <tr>
                  <td><?php echo $value->kd_mk; ?></td>
                  <td><?php echo $value->matakuliah; ?></td>
                  <td><?php echo $value->sks; ?></td>
                  <?php $jurusan = $this->app_model->getdetail('tbl_jurusan','kd_jurusan',$value->kd_jurusan,'kd_jurusan','asc')->row(); ?>
                  <td><?php echo $jurusan->jurusan; ?></td>
                  <td><?php echo $value->semester; ?></td>
                  <td>
                    <div class="btn-group">
                      <button class="btn btn-success" type="button">Aksi</button>
                      <button class="btn btn-success dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="true">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a data-toggle="modal" href="#MyEdit" onclick="edit(<?php echo $value->id_mk; ?>)">Edit</a></li>
                        <li><a onclick="return confirm('Apakah Anda Yakin?');" href="<?php echo base_url();?>admin/matakuliahdelete/<?php echo $value->id_mk; ?>">Delete</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" aria-label="Close" data-dismiss="modal" type="button">
                  <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">FORM</h4>
            </div>
            <form role='form' action="<?php echo base_url();?>admin/matakuliahsave" method="post">
                <div class="modal-body"> 
                <div class="form-group">   
                    <table>
                      <tbody>
                        <tr>
                          <td width="150" align="center">Kode MK</td>
                          <td width="300" style="padding:5px;"><input class="form-control" type="text" placeholder="Kode MK" name="kode" required/></td>
                        </tr>
                        <tr>
                          <td width="150" align="center">Mata Kuliah</td>
                          <td width="300" style="padding:5px;"><input class="form-control" type="text" placeholder="Mata Kuliah" name="mk" required/></td>
                        </tr>
                        <tr>
                          <td width="150" align="center">SKS</td>
                          <td width="300" style="padding:5px;"><input class="form-control" type="number" placeholder="SKS" name="sks" required/></td>
                        </tr>
                        <tr>
                          <td width="150" align="center">Fakultas</td>
                          <td width="300" style="padding:5px;"><select class="form-control" name="fakultas" required>
                            <option disabled selected> -- PILIH -- </option>
                            <?php foreach ($getDataFakultas as $fakultas) {
                              echo "<option value='".$fakultas->kd_fakultas."'> ".$fakultas->fakultas." </option>";
                            } ?>
                          </select></td>
                        </tr>
                        <tr>
                          <td width="150" align="center">Jurusan</td>
                          <td width="300" style="padding:5px;"><select class="form-control" name="jurusan" required>
                            <option disabled selected> -- PILIH -- </option>
                            <?php foreach ($getDataJurusan as $jurusan) {
                              echo "<option value='".$jurusan->kd_jurusan."'> ".$jurusan->jurusan." </option>";
                            } ?>
                          </select></td>
                        </tr>
                        <tr>
                          <td width="150" align="center">Semester</td>
                          <td width="300" style="padding:5px;"><input class="form-control" type="number" placeholder="Semester" name="semester" required/></td>
                        </tr>
                      </tbody>
                    </table>
                </div> 
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default pull-left" data-dismiss="modal" type="button">Close</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="MyEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->