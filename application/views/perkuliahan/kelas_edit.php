<div class="modal-header">
    <button class="close" aria-label="Close" data-dismiss="modal" type="button">
      <span aria-hidden="true">×</span>
    </button>
    <h4 class="modal-title">FORM</h4>
</div>
<form role='form' action="<?php echo site_url(); ?>admin/savekelas" method="post">
  <input type="hidden" name="id" value="<?php echo $kelas; ?>"/>
    <div class="modal-body"> 
    <div class="form-group">   
        <table>
          <tbody>
            <tr>
              <td width="150" align="center">Mahasiswa</td>
              <td width="300" style="padding:5px;">
                <select class="form-control" name="nim" required>
                  <option disabled selected>Pilih Mahasiswa</option>
                  <?php foreach ($getData as $value) { $nim = $value->nim; $nama = $value->nama_mhs;?>
                    
                      <option value="<?php echo $nim;?>"><?php echo $nim.' - '.$nama; ?></option>
                    
                  <?php } ?>
                </select>
              </td>
            </tr>
          </tbody>
        </table>
    </div> 
    </div>
    <div class="modal-footer">
        <button class="btn btn-default pull-left" data-dismiss="modal" type="button">Close</button>
        <input type="submit" class="btn btn-primary" value="Simpan"/>
    </div>
</form>