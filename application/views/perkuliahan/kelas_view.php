<script>
function edit(id){
$('#edit').load('<?php echo base_url();?>admin/kelasedit/'+id);
}

function lihat(id){
$('#edit').load('<?php echo base_url();?>admin/kelaslihat/'+id);
}
</script>

<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Data kelas Mata Kuliah</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            <!--a data-toggle="modal" href="#myModal" ><button class="btn btn-primary btn-flat">+ Tambah Data</button></a><hr-->
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Kode MK</th>
                  <th>Mata Kuliah</th>
                  <th>Hari</th>
                  <th>Jurusan</th>
                  <th>Dosen</th>
                  <th width='80'>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($getData as $value) { ?>
                <tr>
                  <td><?php echo $value->kd_mk; ?></td>
                  <?php $mk = $this->app_model->getdetail('tbl_matakuliah','kd_mk',$value->kd_mk,'kd_mk','asc')->row(); ?>
                  <td><?php echo $mk->matakuliah; ?></td>
                  <td><?php echo $value->hari; ?></td>
                  <?php $jurusan = $this->app_model->getdetail('tbl_jurusan','kd_jurusan',$mk->kd_jurusan,'kd_jurusan','asc')->row(); ?>
                  <td><?php echo $jurusan->jurusan; ?></td>
                  <?php $karyawan = $this->app_model->getdetail('tbl_karyawan','nik',$value->nidn,'nik','asc')->row(); ?>
                  <td><?php echo $karyawan->nama_karyawan; ?></td>
                  <td>
                    <div class="btn-group">
                      <button class="btn btn-success" type="button">Aksi</button>
                      <button class="btn btn-success dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="true">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a data-toggle="modal" href="#MyEdit" onclick="edit(<?php echo $value->id_jadwal; ?>)">Tambah</a></li>
                        <li><a data-toggle="modal" href="#MyEdit" onclick="lihat(<?php echo $value->id_jadwal; ?>)">Lihat</a></li>
                        <!--li><a onclick="return confirm('Apakah Anda Yakin?');" href="<?php echo base_url();?>admin/kelasdelete/<?php echo $value->id_jadwal; ?>">Delete</a></li-->
                      </ul>
                    </div>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div class="modal fade" id="MyEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->