<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class M_login extends CI_Model{

	function cek_user($user, $pass){
		$this->db->select('*');
		return $this->db->get_where('tbl_user_login', 
			array(
				'username' => $user,
				'password' => sha1(md5($pass))
		));
	}

	function cekuser($user,$pass)
	{
		$url = "/user";
		$data = ["username" => $user, "password" => $pass];

		$res = $this->sia->api($url, "POST", $data);

		$user = new stdClass;
		$user->num_rows = 0;

		if (!empty($res)) {
			$user->num_rows = 1;
			$data = $res->data[0];

			$sess_data['id_user'] = $data->userid;
			$sess_data['user_type'] = $data->user_type;
			$this->session->set_userdata($sess_data);

			$user->user_type = $data->user_type;
			return $user;
		}

		return $user;
	}
}