<div class="modal-header">
    <button class="close" aria-label="Close" data-dismiss="modal" type="button">
      <span aria-hidden="true">×</span>
    </button>
    <h4 class="modal-title">FORM</h4>
</div>
<form role='form' action="<?php echo base_url();?>admin/mahasiswaupdate" method="post">
  <input type="hidden" name="id" value="<?php echo $getEdit->id_mhs; ?>"/>
    <div class="modal-body"> 
    <div class="form-group">   
        <table>
          <tbody>
            <tr>
              <td width="150" align="center">NIM</td>
              <td width="300" style="padding:5px;"><input class="form-control" type="text" placeholder="NIM" name="nim" value="<?php echo $getEdit->nim; ?>" required/></td>
            </tr>
            <tr>
              <td width="150" align="center">Nama</td>
              <td width="300" style="padding:5px;"><input class="form-control" type="text" placeholder="Nama" name="nama" value="<?php echo $getEdit->nama_mhs; ?>" required/></td>
            </tr>
            <tr>
              <td width="150" align="center">Telepon</td>
              <td width="300" style="padding:5px;"><input class="form-control" type="text" placeholder="Telepon" name="tlp" value="<?php echo $getEdit->telepon; ?>" required/></td>
            </tr>
            <tr>
              <td width="150" align="center">Email</td>
              <td width="300" style="padding:5px;"><input class="form-control" type="email" placeholder="Email" name="email" value="<?php echo $getEdit->email; ?>" required/></td>
            </tr>
            <tr>
              <td width="150" align="center">Fakultas</td>
              <td width="300" style="padding:5px;"><select class="form-control" name="fakultas" required>
                <option> -- PILIH -- </option>
                <?php foreach ($getDataFakultas as $fakultas) {
                  echo "<option value='".$fakultas->kd_fakultas."'> ".$fakultas->fakultas." </option>";
                } ?>
              </select></td>
            </tr>
            <tr>
              <td width="150" align="center">Jurusan</td>
              <td width="300" style="padding:5px;"><select class="form-control" name="jurusan" required>
                <option> -- PILIH -- </option>
                <?php foreach ($getDataJurusan as $jurusan) {
                  echo "<option value='".$jurusan->kd_jurusan."'> ".$jurusan->jurusan." </option>";
                } ?>
              </select></td>
            </tr>
            <tr>
              <td width="150" align="center">Kelas</td>
              <td width="300" style="padding:5px;"><select class="form-control" name="kelas" required>
                <option disabled selected> -- PILIH -- </option>
                <option value="1"> Reguler - Pagi </option>
                <option value="2"> Reguler - Malem </option>
                <option value="3"> P2K </option>
              </select></td>
            </tr>
          </tbody>
        </table>
    </div> 
    </div>
    <div class="modal-footer">
        <button class="btn btn-default pull-left" data-dismiss="modal" type="button">Close</button>
        <input type="submit" class="btn btn-primary" value="Simpan"/>
    </div>
</form>