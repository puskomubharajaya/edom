<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
 */
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
 */

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');
/**
 * ENV Variable
 */
define('APP_KEY', 'YXBwX2tleV9jaGF0Ym9vdA');
define('API_SIA', 'http://172.16.1.5:8123/api/v1/');
// define('API_SIA', 'http://oldsia.ubharajaya.ac.id/api/v1/');

/*Database Environtment Variable
|
| 172.16.0.75 SIA Live Local
|
|
 */

define('DBHOST', '172.16.0.75');
define('DBUSER', 'tamudb');
define('DBPORT', '3306');
define('DBPASS', 'Mastah@von787566');
define('DBNAME', 'db_newskripsi');

// define('DBHOST', 'localhost');
// define('DBUSER', 'root');
// define('DBPORT', '3306');
// define('DBPASS', '');
// define('DBNAME', 'db_newskripsi');

// define('DBHOST2', 'localhost');
// define('DBUSER2', 'root');
// define('DBPORT2', '3306');
// define('DBPASS2', '');
// define('DBNAME2', 'db_siakadlive');

/* End of file constants.php */
/* Location: ./application/config/constants.php */