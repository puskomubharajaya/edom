<div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Data Eval <?php echo strtoupper($kry->nama); ?> Tahun Ajaran</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <!-- <a data-toggle="modal" href="#myModal" ><button class="btn btn-primary btn-flat">+ Tambah Data</button></a><hr> -->
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Kode MK</th>
                        <th>Mata Kuliah</th>
                        <th>Kelas</th>
                        <th>Nilai Akumulatif</th>
                        <th>Total Input</th>
                        <th>Total Mhs</th>
                        <th width='80'>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $no = 1; foreach ($getData as $key => $value) { ?>
                      <tr>
                        <td><?php echo number_format($no); ?>.</td>
                        <td><?php echo $value->kd_matakuliah; ?></td>
                        <td><?php echo $value->nama_matakuliah; ?></td>
                        <td><?php echo $value->kelas; ?></td>
                        <?php 
                              $idjadwal = $this->db->query("SELECT id FROM tbl_pengisian_kuisioner WHERE kd_jadwal = '".$value->kd_jadwal."'")->row()->id;
                              $rata2 = $this->db->query("SELECT AVG(hasil_input) as akhir FROM tbl_pengisian_kuisioner WHERE kd_jadwal = '".$value->kd_jadwal."'")->row()->akhir;
                              $jmlmhs = $this->db->query("SELECT COUNT(DISTINCT npm_mahasiswa) as akhir FROM tbl_pengisian_kuisioner WHERE kd_jadwal = '".$value->kd_jadwal."'")->row()->akhir;
                        ?>
                        <td><?php echo number_format($rata2,2); ?></td>
                        <td><?php echo $jmlmhs; ?></td>
                        <td><?php echo $value->mhs; ?></td>
                        <td>
                          <div class="btn-group">
                            <button class="btn btn-success" type="button">Aksi</button>
                            <button class="btn btn-success dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="true">
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="<?php echo base_url();?>spi/listdosenajartadetil/<?php echo $idjadwal; ?>">Lihat</a></li>
                              <li><a href="#">Print</a></li>
                            </ul>
                          </div>
                        </td>
                      </tr>
                      <?php $no++; } ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->