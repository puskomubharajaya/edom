<div class="modal-header">
    <button class="close" aria-label="Close" data-dismiss="modal" type="button">
      <span aria-hidden="true">×</span>
    </button>
    <h4 class="modal-title">FORM</h4>
</div>
<form role='form' action="<?php echo base_url();?>admin/matakuliahupdate" method="post">
  <input type="hidden" name="id" value="<?php echo $getEdit->id_mk; ?>"/>
    <div class="modal-body"> 
    <div class="form-group">   
        <table>
          <tbody>
            <tr>
              <td width="150" align="center">Kode MK</td>
              <td width="300" style="padding:5px;"><input class="form-control" type="text" placeholder="Kode MK" name="kode" value="<?php echo $getEdit->kd_mk; ?>" required/></td>
            </tr>
            <tr>
              <td width="150" align="center">Mata Kuliah</td>
              <td width="300" style="padding:5px;"><input class="form-control" type="text" placeholder="Mata Kuliah" name="mk" value="<?php echo $getEdit->matakuliah; ?>" required/></td>
            </tr>
            <tr>
              <td width="150" align="center">SKS</td>
              <td width="300" style="padding:5px;"><input class="form-control" type="number" placeholder="SKS" name="sks" value="<?php echo $getEdit->sks; ?>" required/></td>
            </tr>
            <tr>
              <td width="150" align="center">Fakultas</td>
              <td width="300" style="padding:5px;"><select class="form-control" name="fakultas" required>
                <option disabled selected> -- PILIH -- </option>
                <?php foreach ($getDataFakultas as $fakultas) {
                  echo "<option value='".$fakultas->kd_fakultas."'> ".$fakultas->fakultas." </option>";
                } ?>
              </select></td>
            </tr>
            <tr>
              <td width="150" align="center">Jurusan</td>
              <td width="300" style="padding:5px;"><select class="form-control" name="jurusan" required>
                <option disabled selected> -- PILIH -- </option>
                <?php foreach ($getDataJurusan as $jurusan) {
                  echo "<option value='".$jurusan->kd_jurusan."'> ".$jurusan->jurusan." </option>";
                } ?>
              </select></td>
            </tr>
            <tr>
              <td width="150" align="center">Semester</td>
              <td width="300" style="padding:5px;"><input class="form-control" type="number" placeholder="Semester" name="semester" value="<?php echo $getEdit->semester; ?>" required/></td>
            </tr>
          </tbody>
        </table>
    </div> 
    </div>
    <div class="modal-footer">
        <button class="btn btn-default pull-left" data-dismiss="modal" type="button">Close</button>
        <input type="submit" class="btn btn-primary" value="Simpan"/>
    </div>
</form>