<script>
function edit(id){
$('#edit').load('<?php echo base_url();?>admin/karyawanedit/'+id);
}
</script>

<div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Data Dosen</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <a data-toggle="modal" href="#myModal" ><button class="btn btn-primary btn-flat">+ Tambah Data</button></a><hr>
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>NIDN</th>
                        <th>Nama Dosen</th>
                        <th>Jabatan</th>
                        <th width='80'>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $no = 1; foreach ($getData as $value) { ?>
                      <tr>
                        <td><?php echo number_format($no); ?>.</td>
                        <td><?php echo $value->nik; ?></td>
                        <td><?php echo $value->nama_karyawan; ?></td>
                        <?php $jabatan = $this->app_model->getdetail('tbl_jabatan','kd_jabatan',$value->kd_jabatan,'kd_jabatan','asc')->row(); ?>
                        <td><?php echo $jabatan->jabatan; ?></td>
                        <td>
                          <div class="btn-group">
                            <button class="btn btn-success" type="button">Aksi</button>
                            <button class="btn btn-success dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="true">
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                              <li><a data-toggle="modal" href="#MyEdit" onclick="edit(<?php echo $value->id_dosen; ?>)">Edit</a></li>
                              <li><a onclick="return confirm('Apakah Anda Yakin?');" href="<?php echo base_url();?>admin/karyawandelete/<?php echo $value->id_dosen; ?>">Delete</a></li>
                            </ul>
                          </div>
                        </td>
                      </tr>
                      <?php $no++; } ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" aria-label="Close" data-dismiss="modal" type="button">
                  <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">FORM</h4>
            </div>
            <form role='form' action="<?php echo base_url();?>admin/karyawansave" method="post">
                <div class="modal-body"> 
                <div class="form-group">   
                    <table>
                      <tbody>
                        <tr>
                          <td width="150" align="center">NIK</td>
                          <td width="300" style="padding:5px;"><input class="form-control" type="text" placeholder="NIK" name="nik" required/></td>
                        </tr>
                        <tr>
                          <td width="150" align="center">Nama</td>
                          <td width="300" style="padding:5px;"><input class="form-control" type="text" placeholder="Nama" name="nama" required/></td>
                        </tr>
                        <tr>
                          <td width="150" align="center">Telepon</td>
                          <td width="300" style="padding:5px;"><input class="form-control" type="text" placeholder="Telepon" name="tlp" required/></td>
                        </tr>
                        <tr>
                          <td width="150" align="center">Email</td>
                          <td width="300" style="padding:5px;"><input class="form-control" type="email" placeholder="Email" name="email" required/></td>
                        </tr>
                        <tr>
                          <td width="150" align="center">Jabatan</td>
                          <td width="300" style="padding:5px;"><select class="form-control" name="jabatan" required>
                            <option disabled selected> -- PILIH -- </option>
                            <?php foreach ($getDataJabatan as $jabatan) {
                              echo "<option value='".$jabatan->kd_jabatan."'> ".$jabatan->jabatan." </option>";
                            } ?>
                          </select></td>
                        </tr>
                      </tbody>
                    </table>
                </div> 
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default pull-left" data-dismiss="modal" type="button">Close</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="MyEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->