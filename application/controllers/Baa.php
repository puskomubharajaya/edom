<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Baa extends MY_Controller
{


	public function index()
	{
		$data['page'] = 'home';
		$this->load->view('template', $data);
	}

	function e_val()
	{
		$data['getData'] = $this->app_model->getAllProdi();
		$data['page'] = 'data/prodi_view';
		$this->load->view('template', $data);
	}
}

/* End of file baa.php */
/* Location: ./application/controllers/baa.php */
