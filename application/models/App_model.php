<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class app_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
		$this->siabaru = $this->load->database('sia_baru', TRUE);
	}

	function getdata($table, $key, $order)
	{
		$this->db->order_by($key, $order);
		$q = $this->db->get($table);
		return $q;
	}

	function insertdata($table, $data)
	{
		$q = $this->db->insert($table, $data);
		return $q;
	}

	function getdetail($table, $pk, $value, $key, $order)
	{
		$this->db->where($pk, $value);
		$this->db->order_by($key, $order);
		$q = $this->db->get($table);
		return $q;
	}

	function updatedata($table, $pk, $value, $data)
	{
		$this->db->where($pk, $value);
		$q = $this->db->update($table, $data);
		return $q;
	}

	function deletedata($table, $pk, $value)
	{
		$this->db->where($pk, $value);
		$q = $this->db->delete($table);
		return $q;
	}

	function sawmethode($id, $tahun)
	{
		$actyear = $this->tahunakademik(1);
		$q3 = $this->db->query("SELECT nilai,parameter_id FROM tbl_nilai_parameter WHERE nidn = " . $id . " and tahunajaran = '" . $tahun . "' ")->result();
		$jumlah = $this->db->query("SELECT COUNT(nidn) as nidn FROM tbl_nilai_parameter WHERE nidn = " . $id . " and tahunajaran = '" . $tahun . "' ")->row();
		$kumulatip = 0;
		foreach ($q3 as $nilai) {
			$q4 = $this->db->query("SELECT MAX(nilai) AS Maks FROM tbl_nilai_parameter WHERE parameter_id = " . $nilai->parameter_id . " and tahunajaran = '" . $tahun . "' ")->row();
			$bobot = $this->db->query("SELECT bobot FROM tbl_parameter WHERE id_parameter = " . $nilai->parameter_id . " ")->row();
			$bagi = number_format(($nilai->nilai / 100), 2);
			$hasil = number_format($bagi * $bobot->bobot, 2);

			$kumulatip = $kumulatip + $hasil;
		}
		$kelas = $this->db->query("select id_jadwal from tbl_jadwal_matakuliah where nidn = '" . $id . "' and tahunajaran='" . $tahun . "' ")->result();
		$jumlahmk = 0;
		$datakumulatif = 0;
		foreach ($kelas as $key) {
			$banyakmhs = $this->db->query("select count(nim) as jml_mhs from tbl_kelas where id_jadwal = " . $key->id_jadwal . "")->row();
			$banyak = $jumlah->nidn / $banyakmhs->jml_mhs;
			$nyaris = $kumulatip / $banyak;
			$datakumulatif = $datakumulatif + $nyaris;
			$jumlahmk++;
		}
		$data['akhir'] = $datakumulatif / $jumlahmk;
		//$data['nidn'] = $id;
		//$this->db->insert('tbl_hasil_saw', $data);
		return number_format($data['akhir'], 2);
	}

	function sawmethodemk($id, $tahun, $mk)
	{
		$q3 = $this->db->query("SELECT nilai,parameter_id FROM tbl_nilai_parameter WHERE nidn = " . $id . " and tahunajaran = '" . $tahun . "' and kd_mk = '" . $mk . "'")->result();
		$jumlah = $this->db->query("SELECT COUNT(nidn) as nidn FROM tbl_nilai_parameter WHERE nidn = " . $id . " and tahunajaran = '" . $tahun . "' and kd_mk = '" . $mk . "' ")->row();
		$kumulatip = 0;
		foreach ($q3 as $nilai) {
			$q4 = $this->db->query("SELECT MAX(nilai) AS Maks FROM tbl_nilai_parameter WHERE parameter_id = " . $nilai->parameter_id . " and tahunajaran = '" . $tahun . "' and kd_mk = '" . $mk . "' ")->row();
			$bobot = $this->db->query("SELECT bobot FROM tbl_parameter WHERE id_parameter = " . $nilai->parameter_id . " ")->row();
			$bagi = number_format(($nilai->nilai / 100), 2);
			$hasil = number_format($bagi * $bobot->bobot, 2);

			$kumulatip = $kumulatip + $hasil;
		}
		$kelas = $this->db->query("select id_jadwal from tbl_jadwal_matakuliah where nidn = '" . $id . "' and tahunajaran='" . $tahun . "' and kd_mk = '" . $mk . "' ")->result();
		$jumlahmk = 0;
		$datakumulatif = 0;
		foreach ($kelas as $key) {
			$banyakmhs = $this->db->query("select count(nim) as jml_mhs from tbl_kelas where id_jadwal = " . $key->id_jadwal . "")->row();
			$banyak = $jumlah->nidn / $banyakmhs->jml_mhs;
			$nyaris = $kumulatip / $banyak;
			$datakumulatif = $datakumulatif + $nyaris;
			$jumlahmk++;
		}
		$data['akhir'] = $datakumulatif / $jumlahmk;
		//$data['nidn'] = $id;
		//$this->db->insert('tbl_hasil_saw', $data);
		return number_format($data['akhir'], 2);
	}

	function sawmethodemkkelas($id, $tahun, $mk, $idj)
	{
		$q3 = $this->db->query("SELECT nilai,parameter_id FROM tbl_nilai_parameter WHERE nidn = " . $id . " and tahunajaran = '" . $tahun . "' and kd_mk = '" . $mk . "' and id_jadwal = " . $idj . " ")->result();
		$jumlah = $this->db->query("SELECT COUNT(distinct nidn) as nidn FROM tbl_nilai_parameter WHERE nidn = " . $id . " and tahunajaran = '" . $tahun . "' and kd_mk = '" . $mk . "' and id_jadwal = " . $idj . " ")->row();
		$kumulatip = 0;
		foreach ($q3 as $nilai) {
			$q4 = $this->db->query("SELECT MAX(nilai) AS Maks FROM tbl_nilai_parameter WHERE parameter_id = " . $nilai->parameter_id . " and tahunajaran = '" . $tahun . "' and kd_mk = '" . $mk . "' and id_jadwal = " . $idj . " ")->row();
			$bobot = $this->db->query("SELECT bobot FROM tbl_parameter WHERE id_parameter = " . $nilai->parameter_id . " ")->row();
			$bagi = number_format(($nilai->nilai / 100), 2);
			$hasil = number_format($bagi * $bobot->bobot, 2);

			$kumulatip = $kumulatip + $hasil;
		}
		$kelas = $this->db->query("select id_jadwal from tbl_jadwal_matakuliah where nidn = '" . $id . "' and tahunajaran='" . $tahun . "' and kd_mk = '" . $mk . "' and id_jadwal = " . $idj . " ")->result();
		$jumlahmk = 0;
		$datakumulatif = 0;
		foreach ($kelas as $key) {
			$banyakmhs = $this->db->query("select count(distinct nim) as jml_mhs from tbl_kelas where id_jadwal = " . $key->id_jadwal . "")->row();
			$banyak = $jumlah->nidn / $banyakmhs->jml_mhs;
			$nyaris = $kumulatip / $banyak;
			$datakumulatif = $datakumulatif + $nyaris;
			$jumlahmk++;
		}
		$data['akhir'] = $datakumulatif / $jumlahmk;
		//$data['nidn'] = $id;
		//$this->db->insert('tbl_hasil_saw', $data);
		//var_dump($jumlah);exit();
		return number_format($data['akhir'], 2);
	}

	function nilaiparam($idn, $param)
	{
		$this->db->select('*');
		$this->db->from('tbl_nilai_parameter');
		$this->db->where('parameter_id', $param);
		$this->db->where('nidn', $idn);

		$q = $this->db->get()->result();
		return $q;
	}

	function getdosentahunajaran($id)
	{
		$this->db->distinct();
		$this->db->select('nik,tahunajaran,nama_karyawan');
		$this->db->from('tbl_jadwal_matakuliah a');
		//$this->db->join('tbl_matakuliah b', 'a.kd_mk = b.kd_mk');
		$this->db->join('tbl_karyawan c', 'a.nidn = c.nik');
		$this->db->where('a.nidn', $id);
		//$this->db->like('a.tahunajaran', $tahun);
		return $this->db->get();
	}

	function getdosenajar($id, $tahun)
	{
		$this->db->select('*');
		$this->db->from('tbl_jadwal_matakuliah a');
		$this->db->join('tbl_matakuliah b', 'a.kd_mk = b.kd_mk');
		$this->db->join('tbl_karyawan c', 'a.nidn = c.nik');
		$this->db->where('a.nidn', $id);
		$this->db->like('a.tahunajaran', $tahun);
		return $this->db->get();
	}

	function getmhskelas($id)
	{
		$sql = "SELECT * FROM tbl_mahasiswa e WHERE e.`nim` NOT IN(SELECT c.`nim` FROM tbl_jadwal_matakuliah a
				JOIN tbl_kelas b ON a.id_jadwal = b.`id_jadwal`
				JOIN tbl_mahasiswa c ON b.`nim` = c.`nim`
				WHERE a.id_jadwal = " . $id . ")";
		$q = $this->db->query($sql);
		return $q;
	}

	function cekmhskelas($nim, $kelas)
	{
		$this->db->where('nim', $nim);
		$this->db->where('id_jadwal', $kelas);
		$q = $this->db->get('tbl_kelas');
		return $q;
	}


	function kelasmhs($npm)
	{
		$data = ["npm" => $npm];

		$kelas = new stdClass;
		
		$actyear = $this->tahunakademik(1);
		
		$kode = [];

		$jadwal = $this->db->query("SELECT distinct kd_jadwal from tbl_pengisian_kuisioner where npm_mahasiswa = '" . $npm . "' and tahunajaran = '" . $actyear . "' and kd_jadwal IS NOT NULL")->result();
		foreach ($jadwal as $key) {
			$kode[] = $key->kd_jadwal;
		}

		if (!empty($kode)) {
			$data["kode"] = $kode;
		}
		
		$kelas = $this->_kelas_mhs($data);
		
		return $kelas;
	}

	function getdosenajarta($prodi, $tahun)
	{
		$dosen = new stdClass;

		$sql = "SELECT DISTINCT c.`nama`,c.`nid`,(SELECT SUM(mk.`sks_matakuliah`) AS sks FROM tbl_jadwal_matkul jdl
				JOIN tbl_matakuliah mk ON jdl.`id_matakuliah` = mk.`id_matakuliah`
				WHERE jdl.`kd_dosen` = c.`nid` AND
				jdl.kd_tahunajaran = '" . $tahun . "' AND mk.kd_prodi = '" . $prodi . "'
				AND mk.kd_prodi = '" . $prodi . "' AND (mk.`nama_matakuliah` NOT LIKE '%skripsi%' AND mk.`nama_matakuliah` NOT LIKE '%tesis%' AND mk.`nama_matakuliah` NOT LIKE '%kerja praktek%' AND mk.`nama_matakuliah` NOT LIKE '%magang%')) AS sks FROM tbl_jadwal_matkul b
				JOIN tbl_karyawan c ON b.`kd_dosen` = c.`nid`
				JOIN tbl_matakuliah d ON b.`kd_matakuliah` = d.`kd_matakuliah`
				WHERE mk.`kd_prodi` = '" . $prodi . "' AND b.`kd_tahunajaran` = '" . $tahun . "' AND (d.`nama_matakuliah` NOT LIKE '%skripsi%' AND d.`nama_matakuliah` NOT LIKE '%tesis%' AND d.`nama_matakuliah` NOT LIKE '%kerja praktek%' AND d.`nama_matakuliah` NOT LIKE '%magang%')";
		$list =  $this->siabaru->query($sql);

		if ($list->num_rows() > 0) {
			$dosen = $list->result();
		}

		return $dosen;
	}

	function getdosenajartaprodi($prodi, $tahun, $nid)
	{
		$dosen = new stdClass;

		$sql  = "SELECT a.`kd_matakuliah`,b.`nama_matakuliah`,a.`kd_jadwal`,a.`kelas` FROM tbl_jadwal_matkul a
				JOIN tbl_matakuliah b ON a.`kd_matakuliah` = b.`kd_matakuliah`
				WHERE a.`kd_tahunajaran` = '" . $tahun . "' AND a.`kd_dosen` = '" . $nid . "' AND b.`kd_prodi` = '" . $prodi . "'";
		$list = $this->siabaru->query($sql);

		if ($list->num_rows() > 0) {
			$data = [];

			foreach ($list->result() as $key => $value) {
				$mhs = $this->siabaru->query("SELECT COUNT(DISTINCT npm_mahasiswa) as akhir FROM tbl_krs WHERE kd_jadwal = '" . $value->kd_jadwal . "'")->row()->akhir;
				$data[$key] = $value;
				$data[$key]->mhs = $mhs;
			}

			$response = ['status' => 1, 'message' => 'Success', 'data' => $data];
			$this->_responseJSON(200, $response);
		}
		return $dosen;	
	}

	function sawtunggal($kode)
	{
		$actyear = $this->tahunakademik(1);
		$sql = "SELECT parameter_id,nilai,bobot FROM tbl_nilai_parameter a
				JOIN tbl_parameter b ON a.`parameter_id` = b.`id_parameter`
				WHERE a.`kd_input` = '" . $kode . "' AND tahunajaran = " . $actyear;
		return $this->db->query($sql)->result();
	}

	function hitungmhs($mk, $tahun)
	{
		$sql = $this->db->query("SELECT id_jadwal FROM tbl_jadwal WHERE kd_mk = '" . $mk . "' AND tahunajaran = '" . $tahun . "'")->row();
		return $sql1 = $this->db->query("SELECT COUNT(nim) as jml FROM tbl_kelas WHERE id_jadwal = " . $sql->id_jadwal . " ")->row();
	}

	function getparametertopik()
	{
		$this->db->distinct();
		$this->db->select('topik,parameter,bobot,id_parameter');
		$this->db->from('tbl_parameter a');
		$this->db->join('tbl_topik_parameter b', 'a.kd_topik = b.kd_topik');
		$q = $this->db->get()->result();
		return $q;
	}

	/**
	 * Get All Prodi yang sudah ada di SIA
	 * @return mixed
	 */
	function getAllProdi()
	{
		$prodi = new stdClass();

		$url = "prodi";
		$res = $this->sia->api($url, "GET");

		if (!empty($res)) {
			$prodi = $res->data;
		}

		return $prodi;
	}

	/**
	 * Get Detail Prodi
	 * @return  mixed
	 */
	function prodiDetail($kd_prodi='')
	{
		$prodi = new stdClass;

		$url = "prodi/".$kd_prodi;

		$res = $this->sia->api($url, "GET");

		if (count($res->data) > 0) {
			$prodi = $res->data[0];
		}

		return $prodi;
	}

	/**
	 * Get Prodi By NPM
	 * @param string $npm nomor pokok mahasiswa
	 * @return  int kode prodi
	 */
	public function prodiByNpm($npm='')
	{
		$prodi = "";
		$url = "student/".$npm."/prodi";
		
		$res = $this->sia->api($url, "GET");

		if (!empty($res)) {
			$prodi = $res->data;
		}

		return $prodi;
	}

	/**
	 * Tahun Akademik
	 */
	public function tahunakademik($status = "")
	{

		$sql = "SELECT * FROM tbl_tahunakademik";

		if (!empty($status)) {
			$sql .= " WHERE status = " . $status;
		}


		$list = $this->siabaru->query($sql);

		$tahunakademik = "";

		if ($list->num_rows() > 0) {

			if (!empty($status)) {
				$tahunakademik = $list->row()->kode;
			} else {
				$tahunakademik = $list->result_array();
			}
		}

		return $tahunakademik;
	}

	/**
	 * Detail Dosen
	 * @param  int $nid nomor induk dosen
	 * @return  mixed
	 */
	public function detailDosen($nid='')
	{
		$dosen = new stdClass;

		$endpoint = "dosen/".$nid;

		$res = $this->sia->api($endpoint, "GET");
		if (!empty($res)) {
			$dosen = $res->data;
		}
		return $dosen;
	}

	public function _kelas_mhs($kode="")
	{
		$req = (object) $kode;
		

		$npm = $req->npm;
		$kode = @$req->kode ? $req->kode : [];

		$actyear = getactyear();
		$prodi = $this->prodi_student($npm, FALSE);

		$kodezz = $npm . $actyear;

		$this->siabaru->distinct();
		$this->siabaru->select('c.nama,c.nid,d.nama_matakuliah,b.kd_matakuliah,a.kd_jadwal');
		$this->siabaru->from('tbl_krs a');
		$this->siabaru->join('tbl_jadwal_matkul b', 'a.kd_jadwal = b.kd_jadwal');
		$this->siabaru->join('tbl_karyawan c', 'b.kd_dosen = c.nid');
		$this->siabaru->join('tbl_matakuliah d', 'b.kd_matakuliah = d.kd_matakuliah');
		$this->siabaru->like('a.kd_krs', $kodezz, 'after');
		$this->siabaru->where('d.kd_prodi', $prodi);
		$this->siabaru->where('a.kd_jadwal is NOT NULL', NULL, FALSE);
		$namamk = "d.`nama_matakuliah` NOT LIKE 'skripsi%' AND d.`nama_matakuliah` NOT LIKE '%tesis%' AND d.`nama_matakuliah` NOT LIKE '%kerja praktek%' AND d.`nama_matakuliah` NOT LIKE '%magang%'";
		$this->siabaru->where($namamk);
		if (!empty($kode)) {
			$this->siabaru->where_not_in('a.kd_jadwal', $kode);
		}
		$list = $this->siabaru->get();

		$data = [];
		if ($list->num_rows() > 0) {
			$data = $list->result();
		}

		return $data;
	}

	public function prodi_student($npm='')
	{
		$sql = "SELECT KDPSTMSMHS FROM tbl_mahasiswa WHERE NIMHSMSMHS = '" . $npm . "'";

		$list = $this->siabaru->query($sql);
		
		$prodi = "";

		if ($list->num_rows() > 0) {
			$prodi = $list->row()->KDPSTMSMHS;
		}

		return $prodi;
	}
}

/* End of file app_model.php */
/* Location: ./application/models/app_model.php */
