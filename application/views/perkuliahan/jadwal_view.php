<script>
function edit(id){
$('#edit').load('<?php echo base_url();?>admin/jadwaledit/'+id);
}
</script>

<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Data Kelas Mata Kuliah</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            <a data-toggle="modal" href="#myModal" ><button class="btn btn-primary btn-flat">+ Tambah Data</button></a><hr>
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Kode MK</th>
                  <th>Mata Kuliah</th>
                  <th>Hari</th>
                  <th>Mulai</th>
                  <th>Selesai</th>
                  <th>Dosen</th>
                  <th width='80'>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($getData as $value) { ?>
                <tr>
                  <td><?php echo $value->kd_mk; ?></td>
                  <?php $mk = $this->app_model->getdetail('tbl_matakuliah','kd_mk',$value->kd_mk,'kd_mk','asc')->row(); ?>
                  <td><?php echo $mk->matakuliah; //$value->matakuliah; ?></td>
                  <td><?php echo $value->hari; ?></td>
                  <?php //$jurusan = $this->app_model->getdetail('tbl_jurusan','kd_jurusan',$value->kd_jurusan,'kd_jurusan','asc')->row(); ?>
                  <td><?php echo $value->jam_mulai; ?></td>
                  <td><?php echo $value->jam_selesai; ?></td>
                  <?php $dsn = $this->app_model->getdetail('tbl_karyawan','nik',$value->nidn,'nik','asc')->row(); ?>
                  <td><?php echo $dsn->nama_karyawan; //$value->matakuliah; ?></td>
                  <td>
                    <div class="btn-group">
                      <button class="btn btn-success" type="button">Aksi</button>
                      <button class="btn btn-success dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="true">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a data-toggle="modal" href="#MyEdit" onclick="edit(<?php echo $value->id_jadwal; ?>)">Edit</a></li>
                        <li><a onclick="return confirm('Apakah Anda Yakin?');" href="<?php echo base_url();?>admin/jadwaldelete/<?php echo $value->id_jadwal; ?>">Delete</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" aria-label="Close" data-dismiss="modal" type="button">
                  <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">FORM</h4>
            </div>
            <form role='form' action="<?php echo base_url();?>admin/jadwalsave" method="post">
                <div class="modal-body"> 
                <div class="form-group">   
                    <table>
                      <tbody>
                        <tr>
                          <td width="150" align="center">Dosen</td>
                          <td width="300" style="padding:5px;"><select class="form-control" name="dosen" required>
                            <option disabled selected> -- PILIH -- </option>
                            <?php foreach ($getDataDosen as $dosen) {
                              echo "<option value='".$dosen->nik."'> ".$dosen->nama_karyawan." </option>";
                            } ?>
                          </select></td>
                        </tr>
                        <tr>
                          <td width="150" align="center">Mata Kuliah</td>
                          <td width="300" style="padding:5px;"><select class="form-control" name="mk" required>
                            <option disabled selected> -- PILIH -- </option>
                            <?php foreach ($getDataMk as $mk) {
                              echo "<option value='".$mk->kd_mk."'> ".$mk->matakuliah." </option>";
                            } ?>
                          </select></td>
                        </tr>
                        <tr>
                          <td width="150" align="center">Hari</td>
                          <td width="300" style="padding:5px;">
                            <select class="form-control" name="hari" required>
                              <option value="Senin">Senin</option>
                              <option value="Selasa">Selasa</option>
                              <option value="Rabu">Rabu</option>
                              <option value="Kamis">Kamis</option>
                              <option value="Jumat">Jumat</option>
                              <option value="Sabtu">Sabtu</option>
                              <option value="Minggu">Minggu</option>
                            </select></td>
                        </tr>
                        <tr>
                          <td width="150" align="center">Jam Mulai</td>
                          <td width="300" style="padding:5px;"><input class="form-control" type="text" placeholder="Jam Mulai" name="mulai" required/></td>
                        </tr>
                        <tr>
                          <td width="150" align="center">Jam Selesai</td>
                          <td width="300" style="padding:5px;"><input class="form-control" type="text" placeholder="Jam Selesai" name="selesai" required/></td>
                        </tr>
                      </tbody>
                    </table>
                </div> 
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default pull-left" data-dismiss="modal" type="button">Close</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="MyEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->