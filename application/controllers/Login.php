<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_login');
	}

	public function index()
	{
		if ($this->session->userdata('sess_login') != '') {
			$logged = $this->session->userdata('sess_login');
			$sess_data['id_user'] = $logged['userid'];
			$sess_data['user_type'] = 2;
			$this->session->set_userdata($sess_data);
			redirect('nilai');
		} elseif ($this->session->userdata('id_user') != '') {
			switch ($this->session->userdata('user_type')) {
					// case '1':
					// 	redirect('admin');
					// 	break;
				case '2':
					redirect('nilai');
					break;
					// case '3':
					// 	redirect('spi');
					// 	break;

					// default:
					// 	redirect('nilai');
					// 	break;
			}
		} else {
			$data['title'] = 'Evaluasi Proses Belajar Mengajar';
			$this->load->view('v_login', $data);
		}
	}

	public function masuk()
	{

		$user = $this->input->post('user_name');
		$pass = $this->input->post('user_password');

		$user = $this->m_login->cekuser($user, $pass);

		if ($user->num_rows == 1) {

			if ($user->user_type == '1') {
				redirect('admin');
			} elseif ($user->user_type == '2') {
				redirect('nilai');
			} elseif ($user->user_type == '3') {
				redirect('spi');
			} elseif ($user->user_type == '4') {
				redirect('spi');
			}else{
				$this->session->set_flashdata('pesan', 'Maaf, kombinasi username dan password salah. Ulangi kembali.');
				redirect('login');
			}
		} else {
			$this->session->set_flashdata('pesan', 'Maaf, kombinasi username dan password salah. Ulangi kembali.');
			redirect('login');
		}
	}

	function keluar()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}
}
