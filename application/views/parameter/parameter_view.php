<script>
function edit(id){
$('#edit').load('<?php echo base_url();?>spi/parameteredit/'+id);
}
</script>

<div class="content-wrapper">
  
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Data Parameter</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            <a data-toggle="modal" href="#myModal" ><button class="btn btn-primary btn-flat">+ Tambah Data</button></a><hr>
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Parameter</th>
                  <th>Bobot</th>
                  <th>Topik</th>
                  <th width='80'>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $no = 1; foreach ($getData as $value) { ?>
                <tr>
                  <td><?php echo number_format($no); ?></td>
                  <td><?php echo $value->parameter; ?></td>
                  <td><?php echo $value->bobot; ?></td>
                  <td><?php echo $value->kd_topik; ?>.<?php echo $value->topik; ?></td>
                  <td>
                    <div class="btn-group">
                      <button class="btn btn-success" type="button">Aksi</button>
                      <button class="btn btn-success dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="true">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a data-toggle="modal" href="#MyEdit" onclick="edit(<?php echo $value->id_parameter; ?>)">Edit</a></li>
                        <li><a onclick="return confirm('Apakah Anda Yakin?');" href="<?php echo base_url();?>spi/parameterdelete/<?php echo $value->id_parameter; ?>">Delete</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
                <?php $no++; } ?>
              </tbody>
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" aria-label="Close" data-dismiss="modal" type="button">
                  <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">FORM</h4>
            </div>
            <form role='form' action="<?php echo base_url();?>spi/parametersave" method="post">
                <div class="modal-body"> 
                <div class="form-group">   
                    <table>
                      <tbody>
                        <tr>
                          <td width="150" align="center">Parameter</td>
                          <td width="300" style="padding:5px;"><input class="form-control" type="text" placeholder="Parameter" name="parameter" required/></td>
                        </tr>
                        <tr>
                          <td width="150" align="center">Bobot</td>
                          <td width="100" style="padding:5px;"><input class="form-control" type="number" placeholder="Parameter" name="bobot" required/></td>
                        </tr>
                        <tr>
                          <td width="150" align="center">Topik</td>
                          <td width="300" style="padding:5px;">
                            <select name="kode" class="form-control" required>
                              <option disabled selected> -- Pilih Topik -- </option>
                              <?php foreach ($getTopik as $key) { ?>
                                <option value="<?php echo $key->kd_topik; ?>"><?php echo $key->kd_topik; ?>. <?php echo $key->topik; ?></option>
                              <?php } ?>
                            </select>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                </div> 
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default pull-left" data-dismiss="modal" type="button">Close</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="MyEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->