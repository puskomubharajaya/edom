<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include (FCPATH."/vendor/rmccue/requests/library/Requests.php");
Requests::register_autoloader();

class MY_Controller extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}

}

/* End of file MY_Controller.php */
/* Location: ./application/controllers/MY_Controller.php */