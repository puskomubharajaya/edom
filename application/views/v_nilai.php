<?php include('head.php');?>

<div class="row margin-top-login center-align">
	<h2>
	<span class="light-blue-text">
	<i class="mdi-editor-mode-edit prefix"></i>
	Penilaian
	</span>
	</h2>
</div>

<div class="container center-align" style="width: 1500px;">
<form action="<?php echo base_url();?>nilai/submit_nilai" method="post">
	<div class="input-field white-text white">
	  <select name="dosen" required>
	    <option value="" disabled selected>Pilih Dosen MK</option>
	    <?php
	    foreach($dosen as $dsn){
	    ?>
	    	<option value="<?php echo $dsn->nik.'-'.$dsn->kd_mk;?>"><?php echo $dsn->nama_karyawan;?> - <?php echo $dsn->matakuliah;?></option>
	    <?php
	    }
	    ?>
	  </select>
	</div>

	<table class="centered responsive-table stripped hoverable card">
		<thead>
			<tr>
				<td class="center" colspan="2">Aspek</td>
				<td class="center" colspan="4">Tingkat Kepuasan</td>
			</tr>
			<tr>
				<td colspan="2"></td>
				<td>1</td>
				<td>2</td>
				<td>3</td>
				<td>4</td>				
			</tr>
		</thead>
		<tbody>
			<?php $b = 100; $no = 1; $abjad= 'A'; foreach ($topik as $row) { ?>
			<tr>
				<td><?php echo $abjad; ?><hr></td>
				<td colspan="5"><b><?php echo $row->topik; ?></b><hr></td>
			</tr>
				<?php  
				$noto = 1;
				$a = $this->m_nilai->getdetail($row->kd_topik)->result();
				foreach ($a as $value) { ?>
					<input type="hidden" name="parameter<?php echo $no; ?>" value="<?php echo $value->id_parameter; ?>"/>
					<tr>
						<td><?php echo $noto; ?></td>
						<td style="width: 800px;"><?php echo $value->parameter; ?></td>
						<?php foreach ($skala as $key) { ?>
							<td>
								<p>
								    <input class="with-gap" name="nilai<?php echo $no; ?>" type="radio" value="<?php echo $key->keterangan; ?>" id="fuck<?php echo $b; ?>" required/>
								    <label for="fuck<?php echo $b; ?>"><?php echo $key->skala; ?></label>
								</p>
							</td>
						<? $b++;  } ?>
						<!--td>
							<p>
							    <input class="with-gap" name="nilai1" type="radio" value="25" id="nilai1" required/>
							    <label for="nilai1">25</label>
							</p>
						</td>
						<td>
							<p>
							    <input class="with-gap" name="nilai1" type="radio" value="50" id="nilai2" required/>
							    <label for="nilai2">50</label>
							</p>
						</td>
						<td>
							<p>
							    <input class="with-gap" name="nilai1" type="radio" value="75" id="nilai3" required/>
							    <label for="nilai3">75</label>
							</p>
						</td>
						<td>
							<p>
							    <input class="with-gap" name="nilai1" type="radio" value="90" id="nilai4" required/>
							    <label for="nilai4">100</label>
							</p>
						</td-->
					</tr>
				<?php $no++; $noto++; } ?>
			<?php $abjad++; } ?>
		</tbody>

		<!--tbody>
			<tr>
				<td>Penyajian</td>
				<td>
					<p>
					    <input class="with-gap" name="nilai1" type="radio" value="90" id="nilai1" required/>
					    <label for="nilai1">90</label>
					</p>
				</td>
				<td>
					<p>
					    <input class="with-gap" name="nilai1" type="radio" value="95" id="nilai2" required/>
					    <label for="nilai2">95</label>
					</p>
				</td>
				<td>
					<p>
					    <input class="with-gap" name="nilai1" type="radio" value="100" id="nilai3" required/>
					    <label for="nilai3">100</label>
					</p>
				</td>
			</tr>

			<tr>
				<td>Penguasaan Materi</td>
				<td>
					<p>
					    <input class="with-gap" name="nilai2" type="radio" value="90" id="nilai4" required/>
					    <label for="nilai4">90</label>
					</p>
				</td>
				<td>
					<p>
					    <input class="with-gap" name="nilai2" type="radio" value="95" id="nilai5" required/>
					    <label for="nilai5">95</label>
					</p>
				</td>
				<td>
					<p>
					    <input class="with-gap" name="nilai2" type="radio" value="100" id="nilai6" required/>
					    <label for="nilai6">100</label>
					</p>
				</td>
			</tr>

			<tr>
				<td>Tata Bahasa</td>
				<td>
					<p>
					    <input class="with-gap" name="nilai3" type="radio" value="90" id="nilai7" required/>
					    <label for="nilai7">90</label>
					</p>
				</td>
				<td>
					<p>
					    <input class="with-gap" name="nilai3" type="radio" value="95" id="nilai8" required/>
					    <label for="nilai8">95</label>
					</p>
				</td>
				<td>
					<p>
					    <input class="with-gap" name="nilai3" type="radio" value="100" id="nilai9" required/>
					    <label for="nilai9">100</label>
					</p>
				</td>
			</tr>

			<tr>
				<td>Penampilan</td>
				<td>
					<p>
					    <input class="with-gap" name="nilai4" type="radio" value="90" id="nilai10" required/>
					    <label for="nilai10">90</label>
					</p>
				</td>
				<td>
					<p>
					    <input class="with-gap" name="nilai4" type="radio" value="95" id="nilai11" required/>
					    <label for="nilai11">95</label>
					</p>
				</td>
				<td>
					<p>
					    <input class="with-gap" name="nilai4" type="radio" value="100" id="nilai12" required/>
					    <label for="nilai12">100</label>
					</p>
				</td>
			</tr>
		</tbody-->
	</table>
	<button class="waves-effect waves-light btn" type="submit"><i class="mdi-content-send right"></i>Submit</button>

	<button class="waves-effect waves-light btn" type="reset"><i class="mdi-action-cached right"></i>Reset</button>
</form>
	<button class="waves-effect waves-light btn" onclick="window.location='<?php echo base_url();?>nilai/keluar'"><i class="mdi-av-replay right"></i>Keluar</button>
</div>
	