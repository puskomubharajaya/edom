<div class="modal-header">
                <button class="close" aria-label="Close" data-dismiss="modal" type="button">
                  <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">FORM</h4>
            </div>
            <form role='form' action="<?php echo base_url();?>admin/jadwalupdate" method="post">
              <input type="hidden" name="id" value=""/>
                <div class="modal-body"> 
                <div class="form-group">   
                    <table>
                      <tbody>
                        <tr>
                          <td width="150" align="center">Dosen</td>
                          <td width="300" style="padding:5px;"><select class="form-control" name="" required>
                            <option disabled selected> -- PILIH -- </option>
                            <?php foreach ($getDataDosen as $dosen) {
                              echo "<option value='".$dosen->nik."'> ".$dosen->nama_karyawan." </option>";
                            } ?>
                          </select></td>
                        </tr>
                        <tr>
                          <td width="150" align="center">Mata Kuliah</td>
                          <td width="300" style="padding:5px;"><select class="form-control" name="" required>
                            <option disabled selected> -- PILIH -- </option>
                            <?php foreach ($getDataMk as $mk) {
                              echo "<option value='".$mk->kd_mk."'> ".$mk->matakuliah." </option>";
                            } ?>
                          </select></td>
                        </tr>
                        <tr>
                          <td width="150" align="center">SKS</td>
                          <td width="300" style="padding:5px;"><input class="form-control" type="number" placeholder="SKS" name="" disabled/></td>
                        </tr>
                        <tr>
                          <td width="150" align="center">Hari</td>
                          <td width="300" style="padding:5px;"><input class="form-control" type="text" placeholder="Hari" name="" value="" required/></td>
                        </tr>
                        <tr>
                          <td width="150" align="center">Jam Mulai</td>
                          <td width="300" style="padding:5px;"><input class="form-control" type="text" placeholder="Jam Mulai" name="" value="" required/></td>
                        </tr>
                        <tr>
                          <td width="150" align="center">Jam Selesai</td>
                          <td width="300" style="padding:5px;"><input class="form-control" type="text" placeholder="Jam Selesai" name="" value="" required/></td>
                        </tr>
                      </tbody>
                    </table>
                </div> 
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default pull-left" data-dismiss="modal" type="button">Close</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>