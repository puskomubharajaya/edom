<script>
function edit(id){
$('#edit').load('<?php echo base_url();?>admin/jabatanedit/'+id);
}
</script>

<div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Data Program Studi</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <!-- <a data-toggle="modal" href="#myModal" ><button class="btn btn-primary btn-flat">+ Tambah Data</button></a><hr> -->
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Kode Prodi</th>
                        <th>Program Studi</th>
                        <th width='80'>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $no = 1; foreach ($getData as $value) { ?>
                      <tr>
                        <td><?php echo number_format($no); ?>.</td>
                        <td><?php echo $value->kd_prodi; ?></td>
                        <td><?php echo $value->prodi; ?></td>
                        <td>
                          <div class="btn-group">
                            <button class="btn btn-success" type="button">Aksi</button>
                            <button class="btn btn-success dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="true">
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="<?php echo base_url();?>spi/listtahunajaran/<?php echo $value->kd_prodi; ?>">Lihat</a></li>
                            </ul>
                          </div>
                        </td>
                      </tr>
                      <?php $no++; } ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" aria-label="Close" data-dismiss="modal" type="button">
                  <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">FORM</h4>
            </div>
            <form role='form' action="<?php echo base_url();?>admin/jabatansave" method="post">
                <div class="modal-body"> 
                <div class="form-group">   
                    <table>
                      <tbody>
                        <tr>
                          <td width="150" align="center">Kode Jabatan</td>
                          <td width="300" style="padding:5px;"><input class="form-control" type="text" placeholder="Kode Jabatan" name="kode" required/></td>
                        </tr>
                        <tr>
                          <td width="150" align="center">Jabatan</td>
                          <td width="300" style="padding:5px;"><input class="form-control" type="text" placeholder="Jabatan" name="jabatan" required/></td>
                        </tr>
                      </tbody>
                    </table>
                </div> 
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default pull-left" data-dismiss="modal" type="button">Close</button>
                    <input type="submit" class="btn btn-primary" value="Simpan"/>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="MyEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="edit">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->