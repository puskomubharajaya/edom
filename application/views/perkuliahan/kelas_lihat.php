<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
      $(function () {
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
        });
      });
    </script>

<div class="modal-header">
    <button class="close" aria-label="Close" data-dismiss="modal" type="button">
      <span aria-hidden="true">×</span>
    </button>
    <h4 class="modal-title">FORM</h4>
</div>
<form role='form' action="#" method="post">
    <div class="modal-body">   
       <table id="example2" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th width='150'>NIM</th>
            <th>Nama</th>
            <th width='80'>Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($getData as $value) { ?>
          <tr>
            <td><?php echo $value->nim; ?></td>
            <?php $mhs = $this->app_model->getdetail('tbl_mahasiswa','nim',$value->nim,'nim','asc')->row(); ?>
            <td><?php echo $mhs->nama_mhs; ?></td>
            <td><a href="<?php echo site_url();?>admin/kelasdelete/<?php echo $value->nim.'zz'.$kelas; ?>" onclick="return confirm('Hapus Peserta Kelas ?')" class="btn btn-small btn-danger">Hapus</a></td>
          </tr>
          <?php } ?>
        </tbody>
      </table> 
    </div>
    <div class="modal-footer">
        <button class="btn btn-default pull-left" data-dismiss="modal" type="button">Close</button>
        <input type="submit" class="btn btn-primary" value="Simpan"/>
    </div>
</form>