<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * SIA
 * Untuk koneksi ke API SIA
 * 
 */
class Sia
{
	public function api($endpoint, $method = "GET", $data = [])
	{
		$url = API_SIA.$endpoint;
		$header = ["Content-type" => "application/json", "app-key" => APP_KEY];
		$res = new stdClass;

		if ($method == "GET") {
			$res = Requests::get($url, $header);
		}elseif ($method == "POST") {

			if (!empty($data)){
				$data = json_encode($data);
				$res = Requests::post($url, $header, $data);
			}else{
				$res = Requests::post($url, $header);
			}
		}

		$body = $res->body;
		$data = new stdClass;
		
		if ($body) {
			$data = json_decode($body);
		}

		return $data;
	}
}
