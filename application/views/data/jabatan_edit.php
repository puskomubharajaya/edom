<div class="modal-header">
    <button class="close" aria-label="Close" data-dismiss="modal" type="button">
      <span aria-hidden="true">×</span>
    </button>
    <h4 class="modal-title">FORM</h4>
</div>
<form role='form' action="<?php echo base_url();?>admin/jabatanupdate" method="post">
  <input type="hidden" value="<?php echo $getEdit->id_jabatan; ?>" name="id"/>
    <div class="modal-body"> 
    <div class="form-group">   
        <table>
          <tbody>
            <tr>
              <td width="150" align="center">Kode Jabatan</td>
              <td width="300" style="padding:5px;"><input class="form-control" type="text" value="<?php echo $getEdit->kd_jabatan; ?>" name="kode" placeholder="Kode Jabatan" required/></td>
            </tr>
            <tr>
              <td width="150" align="center">Jabatan</td>
              <td width="300" style="padding:5px;"><input class="form-control" type="text" value="<?php echo $getEdit->jabatan; ?>" name="jabatan" placeholder="Jabatan" required/></td>
            </tr>
          </tbody>
        </table>
    </div> 
    </div>
    <div class="modal-footer">
        <button class="btn btn-default pull-left" data-dismiss="modal" type="button">Close</button>
        <input type="submit" class="btn btn-primary" value="Simpan"/>
    </div>
</form>