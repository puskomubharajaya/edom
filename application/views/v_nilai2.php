<!DOCTYPE html>
<html>
<head>
        <title><?php echo $title; ?></title>
        <link rel="stylesheet" href="<?php echo base_url();?>berkas/css/style.css"/>
        <link rel="stylesheet" href="<?php echo base_url();?>berkas/css/materialize.min.css"/>
        <script src="<?php echo base_url();?>berkas/js/jquery.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>berkas/js/materialize.min.js" type="text/javascript"></script>
        <script type="text/javascript">
        	$(document).ready(function() {
			    $('select').material_select();
			});
        </script>
    </head>
<body style="background: transparent url(<?php echo base_url('assets/shattered-island.gif'); ?>) repeat scroll 0% 0%;">
<div class="row margin-top-login center-align">
	<h2>
	<span class="light-blue-text">
	<i class="mdi-editor-mode-edit prefix"></i>
	<?php echo $title; ?>
	</span>
	</h2>
</div>
<div class="container center-align" style="width: 1200px;">
	<form action="<?php echo base_url();?>nilai/submit_nilai" method="post">
		<div class="input-field cyan">
			<center>
				<p style="text-align:center;">* Skala Penilaian : 1 = Sangat Kurang, 2 = Kurang, 3 = Baik, 4 =  Sangat Baik</p>
				<p style="text-align:center;">Responden <b>WAJIB</b> mengisi kuisioner untuk semua dosen mata kuliah </p>				
			</center>		
		</div>
		<div class="input-field white-text white">
		  <select name="dosen" required>
		    <option disabled selected><b><i>Pilih Dosen MK</i></b></option>
		    <?php if(count($dosen) > 0) {
		    foreach($dosen as $dsn) { ?>
		    	<option value="<?php echo $dsn->nid.'-'.$dsn->kd_jadwal;?>">
		    		<?php echo $dsn->nama.' ('.$dsn->nid.')';?> = <?php echo $dsn->kd_matakuliah.' - '.$dsn->nama_matakuliah;?>
		    	</option>
		    <?php } } ?>
		  </select>
		</div>
		<table class="centered responsive-table stripped hoverable card">
			<thead>
				<tr>
					<td class="center" colspan="2">Aspek</td>
					<td class="center" colspan="4">Tingkat Kepuasan</td>
				</tr>
				<tr>
					<td colspan="2"></td>
					<td>1</td>
					<td>2</td>
					<td>3</td>
					<td>4</td>				
				</tr>
			</thead>
			<tbody>
			<?php $b = 100; $no = 1; $abjad= 'A'; foreach ($topik as $row) { ?>
				<tr>
					<td><b><?php echo $abjad; ?></b><hr></td>
					<td colspan="5"><b><?php echo $row->topik; ?></b><hr></td>
				</tr>
				<?php  
					$noto = 1;
					$a = $this->m_nilai->getdetail($row->kd_topik)->result();
					foreach ($a as $value) { ?>
						<input type="hidden" name="parameter<?php echo $no; ?>" value="<?php echo $value->id_parameter; ?>"/>
						<tr>
							<td><?php echo $noto; ?></td>
							<td style="width: 700px;"><?php echo $value->parameter; ?></td>
				
							<?php foreach ($skala as $key) { ?>
							<td>
								<p>
						    		<input class="with-gap" name="nilai<?php echo $no; ?>" type="radio" value="<?php echo $key->keterangan; ?>" id="fuck<?php echo $b; ?>" required/>
						    		<label for="fuck<?php echo $b; ?>"><?php echo $key->skala; ?></label>
								</p>
							</td>
							<?php $b++;  } ?>
						<?php $no++; $noto++; } ?>
					<?php $abjad++; } ?>
				</tr>
				<!-- <tr>
					<td colspan="6"><b>Deskripsi Perkuliahan</b><hr></td>
				</tr>
				<tr>
					<td><b>*</b></td>
					<td colspan="5"><textarea style="border-style: dashed; border-color: green;" class="with-gap" name="keterangan" required placeholder="Wajib Diisi"></textarea></td>
				</tr> -->
				<tr>
					<td colspan="6"><b>Saran</b><hr></td>
				</tr>
				<tr>
					<td><b>*</b></td>
					<td colspan="5"><textarea style="border-style: dashed; border-color: green;" class="with-gap" name="saran" required placeholder="Wajib Diisi"></textarea></td>
				</tr>
			</tbody>
		</table>
		<button class="waves-effect waves-light btn" type="submit"><i class="mdi-content-send right"></i>Submit</button>
		<button class="waves-effect waves-light btn" type="reset"><i class="mdi-action-cached right"></i>Reset</button>
	</form>
	<!-- <button class="waves-effect waves-light btn" href="#"><i class="mdi-av-send right"></i>Data Evaluasi</button> --><hr>
	<button class="waves-effect waves-light btn" onclick="window.location='<?php echo base_url();?>nilai/keluar'"><i class="mdi-av-replay right"></i>Keluar</button>
</div>
</body>
<script>
  	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  	ga('create', 'UA-99012670-1', 'auto');
  	ga('send', 'pageview');

</script>
</html>