<div class="modal-header">
    <button class="close" aria-label="Close" data-dismiss="modal" type="button">
      <span aria-hidden="true">×</span>
    </button>
    <h4 class="modal-title">FORM</h4>
</div>
<form role='form' action="<?php echo base_url();?>admin/karyawanupdate" method="post">
    <input type="hidden" name="id" value="<?php echo $getEdit->id_dosen; ?>"/>
    <div class="modal-body"> 
    <div class="form-group">   
        <table>
          <tbody>
            <tr>
              <td width="150" align="center">NIK</td>
              <td width="300" style="padding:5px;"><input class="form-control" type="text" placeholder="NIK" name="nik" value="<?php echo $getEdit->nik; ?>" required/></td>
            </tr>
            <tr>
              <td width="150" align="center">Nama</td>
              <td width="300" style="padding:5px;"><input class="form-control" type="text" placeholder="Nama" name="nama" value="<?php echo $getEdit->nama_karyawan; ?>" required/></td>
            </tr>
            <tr>
              <td width="150" align="center">Telepon</td>
              <td width="300" style="padding:5px;"><input class="form-control" type="text" placeholder="Telepon" name="tlp" value="<?php echo $getEdit->telepon; ?>" required/></td>
            </tr>
            <tr>
              <td width="150" align="center">Email</td>
              <td width="300" style="padding:5px;"><input class="form-control" type="email" placeholder="Email" name="email" value="<?php echo $getEdit->email; ?>" required/></td>
            </tr>
            <tr>
              <td width="150" align="center">Jabatan</td>
              <td width="300" style="padding:5px;"><select class="form-control" name="jabatan" required>
                <?php $jab = $this->app_model->getdetail('tbl_jabatan','kd_jabatan',$getEdit->kd_jabatan,'kd_jabatan','asc')->row(); ?>
                <option value="<?php echo $getEdit->kd_jabatan; ?>"> <?php echo $jab->jabatan; ?> </option>
                <option disabled> -- PILIH -- </option>
                <?php foreach ($getDataJabatan as $jabatan) {
                  echo "<option value='".$jabatan->kd_jabatan."'> ".$jabatan->jabatan." </option>";
                } ?>
              </select></td>
            </tr>
          </tbody>
        </table>
    </div> 
    </div>
    <div class="modal-footer">
        <button class="btn btn-default pull-left" data-dismiss="modal" type="button">Close</button>
        <input type="submit" class="btn btn-primary" value="Simpan"/>
    </div>
</form>