<?php if (!defined('BASEPATH')) {
    exit('No direct script allowed');
}

class Nilai extends MY_Controller {
    /**
     * @var mixed
     */
    private $actyear;

    public function __construct() {
        parent::__construct();
        $this->load->model('m_nilai');
        $this->load->model('m_presenter');
        if (($this->session->userdata('id_user') == '') or ($this->session->userdata('id_user') != TRUE) or (is_null($this->session->userdata('id_user')))) {
            echo "
                <script>alert('Maaf, akses tidak diijinkan.')</script>
                <script>window.location='http://edom.ubharajaya.ac.id/'</script>
            ";
        }
        $this->actyear = $this->app_model->tahunakademik(1);
    }

    function index() {
        $npm           = $this->session->userdata('id_user');
        $data['title'] = 'Pengisian Kuisioner Evaluasi Belajar Mengajar';
        $data['topik'] = $this->m_nilai->gettopik()->result();
        $data['skala'] = $this->app_model->getdata('tbl_skala', 'skala', 'asc')->result();
        $data['dosen'] = $this->app_model->kelasmhs($npm);

        $this->load->view('v_nilai2', $data);
    }

    function submit_nilai() {
        $pecah = explode('-', $this->input->post('dosen', TRUE));

        $hitung = $this->db->query("SELECT COUNT(id_parameter) AS id_parameter FROM tbl_parameter")->row();
        $sql    = "DELETE from tbl_nilai_parameter where kd_input = '" . md5($pecah[1] . $pecah[0] . $this->session->userdata('id_user')) . "' AND tahunajaran ='" . $this->actyear . "'";

        $this->db->query($sql);

        $sql = "DELETE from tbl_pengisian_kuisioner where kd_input = '" . md5($pecah[1] . $pecah[0] . $this->session->userdata('id_user')) . "' AND tahunajaran = '" . $this->actyear . "'";

        $this->db->query($sql);

        #insert data pengisian -> kuisioner
        $data['npm_mahasiswa'] = $this->session->userdata('id_user');
        $data['kd_jadwal']     = $pecah[1];
        $data['date_input']    = date('Y-m-d H:i:s');
        $data['nid']           = $pecah[0];
        $data['tahunajaran']   = $this->actyear;
        $data['kd_input']      = md5($pecah[1] . $pecah[0] . $this->session->userdata('id_user'));
        $data['saran']         = $this->input->post('saran', TRUE);
        $data['tahunajaran']   = $this->actyear;

        for ($i = 1; $i <= $hitung->id_parameter; $i++) {
            $data1[] = [
                'kd_input'     => $data['kd_input'],
                'parameter_id' => $this->input->post('parameter' . $i . '', TRUE),
                'kd_jadwal'    => $pecah[1],
                'nilai'        => $this->input->post('nilai' . $i . '', TRUE),
                'tahunajaran'  => $this->actyear,
            ];
        }

        if (($this->session->userdata('id_user') == '') or ($this->session->userdata('id_user') != TRUE) or (is_null($this->session->userdata('id_user')))) {
            echo "
                <script>alert('Maaf, akses tidak diijinkan.')</script>
                <script>window.location='http://edom.ubharajaya.ac.id/'</script>
            ";
        } else {
            $this->db->insert_batch('tbl_nilai_parameter', $data1);
            $data['hasil_input'] = $this->hasiltunggal($data['kd_input'], substr($pecah[1], 0, 5));
            $data['ip_address']  = $this->input->ip_address();
            $this->db->insert('tbl_pengisian_kuisioner', $data);

            echo "<script>alert('Berhasil');document.location.href='" . base_url() . "nilai';</script>";
        }
    }

    /**
     * @param $kode
     * @param $prodi
     * @return mixed
     */
    function hasiltunggal($kode, $prodi) {
        $q     = $this->app_model->sawtunggal($kode);
        $hasil = 0;
        foreach ($q as $value) {
            $nilai = $value->nilai * $value->bobot;
            $hasil = $hasil + $nilai;
        }
        $hasilakhir = $hasil / 4;

        return $hasilakhir;
    }

    //metode saw
    function metodesaw() {
        //hitung jumlah dosen dan pecah dosen siapa aja dan bikin kode
        $q2 = $this->db->query("SELECT DISTINCT nidn FROM tbl_nilai_parameter where tahunajaran =" . $this->actyear)->result();
        $a  = 'A1';
        foreach ($q2 as $key) {
            echo "<br/>";
            echo $key->nidn . " = " . $a;
            echo "<br/>";
            $q3        = $this->db->query("SELECT nilai,parameter_id FROM tbl_nilai_parameter WHERE nidn = " . $key->nidn . " AND tahunajaran =  " . $this->actyear)->result();
            $kumulatip = 0;
            foreach ($q3 as $nilai) {
                echo $nilai->nilai . " | saw = ";
                $q4    = $this->db->query("SELECT MAX(nilai) AS Maks FROM tbl_nilai_parameter WHERE parameter_id = " . $nilai->parameter_id . " AND tahunajaran = " . $this->actyear)->row();
                $bobot = $this->db->query("SELECT bobot FROM tbl_parameter WHERE parameter_id = " . $nilai->parameter_id . " ")->row();
                $bagi  = number_format(($nilai->nilai / $q4->Maks), 2);
                $hasil = number_format($bagi * $bobot->bobot, 2);
                echo $hasil;
                echo "<br/>";
                $kumulatip = $kumulatip + $bagi;

                //insert ke table hasil perhitungan saw
                //$this->db->insert('', $data);
            }
            echo "Hasil Akhir = " . number_format($kumulatip, 2);
            echo "<br/>";
            $a++;
        }
    }

    function keluar() {
        $this->session->sess_destroy();
        redirect(base_url());
    }
}
